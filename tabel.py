subjects=["Mathematics|","Computer-Science|","English|","Biology|","Geography|","History|","Arts|","Chemistry|","Physics|","PE"]
tab_rows=["Index","Name - Surname"]
for sub in subjects:
	tab_rows.append(sub)

t_r_d="{} {:^35} {} {} {} {} {} {} {} {} {} {}".format(tab_rows[0],tab_rows[1],tab_rows[2],tab_rows[3],tab_rows[4],tab_rows[5],tab_rows[6],tab_rows[7],tab_rows[8],tab_rows[9],tab_rows[10],tab_rows[11])

sep='~'*len(t_r_d)
tabfin=[]
tabfin=sep
tabfin+='\n'+t_r_d
tabfin+='\n'+sep

grades={}
grades["Ana Maria Dunca"]=[10,10,10,10,10,10,10,10,10,10]
grades["Corina Timbea"]=[9,9,9,9,9,9,9,9,9,9]
grades["Grigore Cojan"]=[8,8,8,8,8,8,8,8,8,8]
grades["Marius Dumitrescu"]=[7,7,7,7,7,7,7,7,7,7]
grades["Petunia Sarajan"]=[6,6,6,6,6,6,6,6,6,6]
grades["Nicusor Santimbru"]=[5,5,5,5,5,5,5,5,5,5]
grades["Maria Molnar"]=[4,4,4,4,4,4,4,4,4,4]
grades["Loren Baciu"]=[3,3,3,3,3,3,3,3,3,3]
grades["Mihai Constantinescu"]=[2,2,2,2,2,2,2,2,2,2]
grades["Roxana Chelarescu"]=[1,1,1,1,1,1,1,1,1,1]

table=""
table+=tabfin+'\n'
index=1

for g in grades:
	form="{:^7} {:^30} {:9} {:15} {:13} {:8} {:8} {:8} {:8} {:8} {:9} {:5}".format(index,g,grades[g][0],grades[g][1],grades[g][2],grades[g][3],grades[g][4],grades[g][5],grades[g][6],grades[g][7],grades[g][8],grades[g][9])
	index+=1
	table+=form+'\n'

print(table)
